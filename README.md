# Send_Email_With_Nodemailer

This project is designed to demonstrate how one can send mail using Nodemailer.

# Blog

Highly recommended to read the blog on [medium](https://medium.com/@urvilnaik003/how-to-send-email-using-nodemailer-f4c94775553d)

# Prerequisite

install [nodejs](https://nodejs.org/en/) to your computer

# Setup

After clone the repository
```
npm install
```
To start the script
```
npm start
```

# Usage

```
.ENV FILE --->
                NODEMAILER_USER = Your_Email_Address_From_Which_You_Want_Send_Email (without @gmail.com)
                NODEMAILER_PASS = Your_Email_Password_From_Which_You_Want_Send_Email
```

# Keynote

One must have to allow access to less secure apps in google account setting. This setting is required to be turned on in the account from which you want to send an email.

```
Go to google account → Security → Less secure app access → Turn on
```

# Route
```
HTTP: POST
PATH: /sendEmail
REQ.BODY: {
    "email": "The email address of person to whom you want to send email"
}
```

# Example steps

If person with email address abc@gmail.com wants to send email to person with email address xyz@gmail.com:-
```
        1-> npm install 
        2-> .ENV FILE ---> 
                            NODEMAILER_USER = abc (without @gmail.com)
                            NODEMAILER_PASS = *****
        3-> npm start or node app.js
        4-> open postman --->
                            URL: localhost:8000/sendEmail
                            METHOD: post
                            BODY: {
                                "email": "xyz@gmail.com"
                            }
```
