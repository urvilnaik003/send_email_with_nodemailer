require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const { transporter, mailOptions } = require('./mail_config');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.post('/sendEmail', async (req, res) => {
    try {
        mailOptions.to = req.body.email;
        await transporter.sendMail(mailOptions);
        res.status(200).json({ message: `Email has been sent to ${req.body.email}.` });
    } catch (error) {
        res.status(500).json({ error: error.message || 'something went wrong' });
    }
})

const port = process.env.PORT || 3000;

app.listen(port, () => {
    console.log(`server listening on ${port}`);
})